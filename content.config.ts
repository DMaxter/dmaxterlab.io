import { defineContentConfig, defineCollection, z } from "@nuxt/content";

export default defineContentConfig({
  collections: {
    resume: defineCollection({
      type: "data",
      source: "main.yml"
    }),
    projects: defineCollection({
      type: "page",
      source: "projects/*.md"
    }),
    writeups: defineCollection({
      type: "page",
      schema: z.object({
        date: z.date()
      }),
      source: "writeups/*.md"
    })
  }
})
