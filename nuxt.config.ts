import { fileURLToPath } from 'url'
import svgLoader from 'vite-svg-loader'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  alias: {
    '@': fileURLToPath(new URL('/', import.meta.url)),
    '@components': fileURLToPath(new URL('/components', import.meta.url)),
    '@images': fileURLToPath(new URL('/public/static', import.meta.url)),
    '@styles': fileURLToPath(new URL('/styles', import.meta.url))
  },
  compatibilityDate: '2024-09-16',
  content: {
    build: {
      markdown: {
        highlight: {
          theme: 'material-theme-lighter',
          langs: ['bash', 'c', 'dockerfile', 'gdscript', 'groovy', 'ini', 'java', 'js', 'json', 'makefile', 'php', 'python', 'rust', 'sql', 'toml', 'xml', 'yaml']
        }
      }
    }
  },
  devtools: { enabled: true },
  modules: ['@nuxt/content', '@nuxt/devtools', '@nuxt/image'],
  nitro: {
    preset: 'github_pages',
    output: {
      publicDir: 'dist'
    },
    serveStatic: true
  },
  vite: {
    esbuild: {
      tsconfigRaw: {}
    },
    plugins: [
      svgLoader({
        svgo: false
      })
    ],
  }
})
