---
name: CDR-Safilo Platform
summary: Platform for Safilo commercials place orders to Casa dos Reclamos
image: /static/icons/casadosreclamos.png
---

# CDR-Safilo Platform

**Link:** <https://github.com/DMaxter/cdr-safilo>

[Casa dos Reclamos](https://www.casadosreclamos.com/) (CDR), a marketing company, **was in need of platform to handle multiple requests** from [Safilo](https://www.safilogroup.com/)'s commercials, so 2 other people and me gathered to develop it, with responsiveness in mind, since **they wanted to use it on the tablets when on a customer shop**.

I handled all of the **design**, **architecture** and **backend development** of the platform, using MySQL for the database and [Quarkus](https://quarkus.io/) with Kotlin for the platform itself.
I also setup the whole DevOps process behind it, in order to automate its deployment. It uses Ansible for provisioning and GitHub Actions for the pipeline.

Later, the team was reduced to 2 people and **I am now also the project leader and manager**, selecting the features we should do next and aligning with Casa dos Reclamos and Safilo on improvements and problems.

The platform currently sends an email to a list of people defined by CDR, when there is a new request, allows Safilo commercials to edit and cancel requests, add photos of their clients and allows Safilo managers to manage the clients, and their photos. CDR can also add the list of materials, prices and finishings on the platform.

**It is also integrated with [FEMA](https://www.fema.pt/)**, a distribution company, since CDR uses it to ship the requests to Safilo's customers, **allowing CDR to improve the process of fulfilling requests**. Currently it is still not implemented, but **we plan to integrate also the tracking API, allowing Safilo's commercials to track their requests**.

Since I am currently facing a lot of problems with the backend:
* Tests not possible;
* Slowness;
* Runtime errors;
* Upgrade to Quarkus 3 not possible due to query errors (because our queries are complex);
* MySQL non terminated sessions leading to errors;

**We will be moving the whole backend to Rust** to make it **more stable** and have **more control over what is being performed**. The platform should become **more responsive** and should **require less maintenance**, while allowing us to **perform unit tests before putting it in production**.
