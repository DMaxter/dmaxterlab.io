---
name: Azure DevOps Playground
summary: Azure DevOps different agent types playground
image: static/icons/azure-devops.png
---

# Azure DevOps Playground

**Link:** <https://gitlab.com/ansible-labs/azure-devops>

At Siemens, I am an Azure DevOps administrator, taking care of multiple build agents of a build farm.
Some agents are of XAML type and the others are of type Azure Pipelines (AZP or VSTS), and **I needed a playground to perform all kinds of experiments at will** to learn more about the system.

I developed this Ansible repo to ramp up a similar environment to what I have and learn everything about Azure DevOps, and **all the quirks with XAML builds and AZP builds**.
With it I was able to **analyze the communication between agents and DevOps server** and have a better understanding of the whole environment.
It really helped in assimilating the whole build system and trying some builds to see the process from start to end.
