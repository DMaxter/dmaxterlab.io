---
name: Simple CRM
summary: Configurable CRM software to manage product selling
image: /static/icons/simple-crm.svg
---

# Simple CRM

**Link:** <https://gitlab.com/DMaxter/simple-crm>

This project started due to a need I had: **I needed a software that would allow me to quickly select products from a pricing list** to make a request and **show me the total value**.
I also needed a **quick way to calculate the change** since I was working only with cash and I did not want to delay people.

With this in mind, I decided to create a <u>configurable backend</u> which would be configured through a YAML file, allowing me to **easily change the product list, prices and images**. I went with **Rust** for it due to the **robustness of the language**, since I did not want my program to suddenly fail in the middle of operation, because it would mean people would be waiting for me to restart or fix the problem.

For the frontend, I used **Vue with TypeScript**, because it is the most suitable framework (in my opinion) for this type of application and with the only sane way to program JavaScript.

In order to create a single binary, I needed to embed the frontend in the final binary (the backend) to make it **easier to distribute**.
To fulfill this, I used [rust-embed](https://crates.io/crates/rust-embed).
It allows me to use the local directory when compiling in debug mode and bundling the frontend when compiling in release mode, and it was really simple to configure.

Since I work as a DevOps engineer, **I also set up a complete GitLab CI workflow** that **builds and lints** both the frontend and the backend if a merge request is opened to the default branch, and also **uploads the compiled binary and creates a release** if a new tag is created.

It is still in a very initial stage, but it has already been proven to be very useful and I believe it can grow and mature to an even better product.
